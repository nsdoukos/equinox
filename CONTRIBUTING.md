# How to contribute
## Coding Style

### Header Files inclusion
* Include first all the Equinox header files
* Third party libraries should be included after, except in cases where this
is not possible. 
* STL or standard library header files should be placed at the end of the 
list of included libraries.
* All Equinox related header files from the public API should be included with
the system header files inclusion method
(e.g `#include <equinox/core/mm/mm.h>`).
The CMake build system is responsible for setting properly the included 
directories during the building process.

### Class Definitions
* Class definitions are placed on header files only. One class definition
per file is allowed.
* In the class definition, public members are declared first, followed by
protected and private, if any.
* If the class should exploit its API outside the scope of the module, then
the header file is places on the `module_name/include/module_name` folder.
Otherwise it is placed on the `module_name/lib` folder.

### Unit testing
* Unit testing is perfomed using [CPPUnit](http://cppunit.sourceforge.net/) 
and CTest suite.
* Each module should include a Unit test that checks the individual components
of the module. The naming scheme of the unit test should have the prefix `test_`
followed by the module name.
* Each module should contain only one testing executable. The individual 
components are registered on the module testing executable using the
`CPPUNIT_TEST_SUITE_REGISTRATION()` CPPUnit macro. For example:

```cpp
CPPUNIT_TEST_SUITE_REGISTRATION(eqnx::core::test_circ_buffer);
CPPUNIT_TEST_SUITE_REGISTRATION(eqnx::core::test_msg_queue);
CPPUNIT_TEST_SUITE_REGISTRATION(eqnx::core::test_mem_pool);

int
main (int argc, char **argv)
{
  CppUnit::TextUi::TestRunner runner;
  CppUnit::TestFactoryRegistry &registry =
      CppUnit::TestFactoryRegistry::getRegistry ();
  runner.addTest (registry.makeTest ());
  bool success = runner.run ("", false);
  return success ? 0 : 1;
}
```
* For each module Unit tests are placed in a seperate directory called `test`.
* Unit test files should start with the prefix `test_`. For examle the 
corresponding Unit test for class `circ_buffer` should be the 
`test_circ_buffer`.
* Unit tests are registered to the CTest suite using the CMake macro 
`add_test()`.
* Evert Unit test should also be registered for memory leak testing using the
custom CMake macro `add_memtest()`. 


## Documentation
Documentation of Equinox is auto-generated using the Doxygen tool. For best
results the following conventions should be used when documenting different
components of the platform.
* All public interfaces should have Doxygen documentation, describing their
purpose and their parameters.
* Structures, enumerations and their members should always have documentation using
the special style provided by Doxygen.
* Often, religion wars break out about where the Doxygen documentation should
be placed. Some prefer on header files whereas other people put them on 
implementation files. While both approaches are legitimate, we follow a scheme
that tries to utilize the advantages of both approaches. Doxygen documentation
is written in header files only for class description, structures and
enumerations. In addition, documentation is written in the header files for those
methods that are pure virtual. In all other case, the Doxygen documentation
should be placed in the implementation file. The reasoning behind this approach
is to avoid people changing frequently the headers files and their interfaces.
Moreover, with this scheme re-compilation size is kept minimal. 

## CMake
Each module of Equinox platform should be places in separated folder following
the GNU directory scheme. Each module should contain a top level `CMakelists.txt`
file. 

Each `CMakelists.txt` file should conform with the following guidelines:
* CMake native commands should be lower-case
* All custom variables with non-local scope should be upper-case
* All custom macros should be also upper-case where functions lowe-case
* CMake custom modules are placed on the `cmake` directory following a camel 
case naming convention, starting with a capital letter. Furthermore, they should
start with the word `Eqnx`. E.g: `EqnxExample.cmake`
* Especially for custom CMake modules for finding packages or dependencies, they
should start with `Find`. E.g: `FindExample.cmake`