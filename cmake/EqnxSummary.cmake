#  Equinox: SDR platform for realtime applications
#  Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

if(DEFINED __INCLUDED_EQNX_SUMMARY_CMAKE)
    return()
endif()
set(__INCLUDED_EQNX_SUMMARY_CMAKE TRUE)

macro(EQNX_PRINT_SUMMARY)
  message(STATUS "Runtime directory   : ${CMAKE_INSTALL_PREFIX}/${EQNX_RUNTIME_DIR}")
  message(STATUS "Libraries directory : ${CMAKE_INSTALL_PREFIX}/${EQNX_LIBRARY_DIR}")
  message(STATUS "Include directory   : ${CMAKE_INSTALL_PREFIX}/${EQNX_INCLUDE_DIR}")
  message(STATUS "Data directory      : ${CMAKE_INSTALL_PREFIX}/${EQNX_DATA_DIR}")
  message(STATUS "")
  message(STATUS "Unit tests          : ${EQNX_ENABLE_TESTING}")
  message(STATUS "Memory tests        : ${EQNX_ENABLE_MEMCHECK}")
  message(STATUS "Benchmarks          : ${EQNX_ENABLE_BENCHMARK}")
  message(STATUS "")
  message(STATUS "Building for version: ${VERSION} / ${LIBVER}")
  message(STATUS "")
endmacro(EQNX_PRINT_SUMMARY)