#  Equinox: SDR platform for realtime applications
#  Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


###############################################################################
# CMake module for the libnuma library
# This module defines the variables:
#    NUMA_FOUND: True if the library found, undefined otherwise
#    NUMA_INCLUDE_DIR: The include directory
#    NUMA_LIBRARY: The Numa library
###############################################################################
FIND_PATH(NUMA_INCLUDE_DIR numa.h)
FIND_LIBRARY(NUMA_LIBRARY NAMES numa)

if (NUMA_INCLUDE_DIR AND NUMA_LIBRARY)
    set(NUMA_FOUND TRUE)
endif (NUMA_INCLUDE_DIR AND NUMA_LIBRARY)

if (NUMA_FOUND)
    if (NOT Numa_FIND_QUIETLY)
        message(STATUS "Found libnuma library: ${NUMA_LIBRARY}")
    endif (NOT Numa_FIND_QUIETLY)
else (NUMA_FOUND)
    if (Numa_FIND_REQUIRED)
        message (FATAL_ERROR "Could not find libnuma library")
    endif (Numa_FIND_REQUIRED)
endif (NUMA_FOUND)