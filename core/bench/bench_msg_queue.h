/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_BENCH_BENCH_MSG_QUEUE_H_
#define CORE_BENCH_BENCH_MSG_QUEUE_H_

#include <benchmark/benchmark.h>

#include <equinox/core/mm/msg_queue.h>
#include <equinox/core/mm/msg_queue_simple.h>
#include <equinox/core/mm/mem_pool.h>
#include <equinox/core/mm/mem_pool_simple.h>
#include <equinox/core/mm/mem_pool_r.h>
#include "../include/equinox/core/mm/msg_queue_r.h"

class bench_msg_queue : public benchmark::Fixture {
protected:
  eqnx::core::msg_queue *d_q;
};


#endif /* CORE_BENCH_BENCH_MSG_QUEUE_H_ */
