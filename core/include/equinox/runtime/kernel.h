/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_RUNTIME_KERNEL_H_
#define CORE_INCLUDE_EQUINOX_RUNTIME_KERNEL_H_

#include <equinox/core/core.h>
#include <equinox/runtime/input_port.h>
#include <equinox/runtime/output_port.h>
#include <equinox/core/sched/connection_anchor.h>

#include <string>
#include <map>

namespace eqnx
{

  class kernel : public std::enable_shared_from_this<kernel>
  {
  public:
    typedef std::shared_ptr<kernel>     sptr;

    static sptr
    make_shared(const std::string& name);

    const std::string&
    name () const;



    /**************************************************************************
     *************************   IO port management   *************************
     *************************************************************************/
    void
    new_input_port (const std::string& name);

    void
    new_output_port (const std::string& name, size_t msg_num = 128,
                     size_t msg_size = 1024);

    input_port::sptr
    input (const std::string& name);

    output_port::sptr
    output (const std::string& name);

    size_t
    input_num ();

    size_t
    output_num ();

    bool
    is_source ();

    bool
    is_sink ();

    friend std::ostream&
    operator<< (std::ostream& out, const kernel& y);

    core::connection_anchor
    operator[] (const std::string& port_name);

    core::connection_anchor
    port(const std::string& port_name);

    virtual
    ~kernel () = 0;

  protected:
    kernel (const std::string& name);

  private:
    const std::string                           d_name;
    /**
     * Initially set to false. When the kernel configuration is finished and
     * prior the run() method first invocation, this field is set to true.
     *
     * This will prevent the configuration methods to be called during runtime.
     */
    bool                                        d_running;
    std::map<std::string, input_port::sptr>     d_in_ports;
    std::map<std::string, output_port::sptr>    d_out_ports;
  };

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_RUNTIME_KERNEL_H_ */
