/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/runtime/io_port.h>

namespace eqnx
{

  io_port::io_port (const std::string& name, io_port_dir_t dir, bool async) :
          d_is_ready (false),
          d_name (name),
          d_id (-1),
          d_dir (dir),
          d_async (async)
  {
  }


  io_port::~io_port() {};

  /**
   *
   * @return true if the IO port has been successfully configured and it is
   * ready for use
   */
  bool
  io_port::is_ready ()
  {
    return d_is_ready;
  }

  std::string
  io_port::name ()
  {
    return d_name;
  }

  size_t
  io_port::id ()
  {
    return d_id;
  }

  io_port::io_port_dir_t
  io_port::get_direction ()
  {
    return d_dir;
  }

  bool
  io_port::is_input ()
  {
    return d_dir == IO_PORT_DIR_INPUT;
  }

  bool
  io_port::is_output ()
  {
    return d_dir == IO_PORT_DIR_OUTPUT;
  }

  bool
  io_port::is_async ()
  {
    return d_async;
  }

  void
  io_port::set_id (size_t id)
  {
    d_id = id;
  }

  std::ostream&
  operator<< (std::ostream& out, const io_port& p)
  {
    return out << p.d_name;
  }
} /* namespace eqnx */
