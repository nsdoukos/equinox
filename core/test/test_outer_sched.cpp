/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "test_outer_sched.h"
#include <equinox/runtime/runtime_exception.h>

namespace eqnx
{

  namespace core
  {

    /*
     * FIXME: For now, use two dummy kernels. Later one use some of the
     * platform provided
     */
    class test_kernel : public virtual kernel
    {
    public:
      typedef std::shared_ptr<test_kernel> sptr;

      test_kernel (const std::string& name) :
        kernel(name)
      {
        new_input_port("in0");
        new_input_port("in1");
      }

      ~test_kernel () {}

      static sptr
      make_shared(const std::string& name)
      {
        return std::shared_ptr<test_kernel> (new test_kernel(name));
      }
    };

    class test_kernel2 : public virtual kernel
    {
      public:
      typedef std::shared_ptr<test_kernel2> sptr;

      test_kernel2 (const std::string& name) :
        kernel(name)
      {
        new_output_port("out", 1024, 1024);
      }

      ~test_kernel2 () {}

      static sptr
      make_shared(const std::string& name)
      {
        return std::shared_ptr<test_kernel2> (new test_kernel2(name));
      }
    };

    void
    test_outer_sched::setUp ()
    {
      d_outer_sched = std::shared_ptr<outer_sched> (new outer_sched (1));
    }

    void
    test_outer_sched::tearDown ()
    {
    }

    void
    test_outer_sched::create_connections ()
    {
      input_port::sptr test_in;
      output_port::sptr test_out;
      connection_anchor anch0;
      connection_anchor anch1;
      connection::sptr conn;

      test_kernel::sptr sink = test_kernel::make_shared("sink");
      test_kernel2::sptr source0 = test_kernel2::make_shared("source0");
      test_kernel2::sptr source1 = test_kernel2::make_shared("source1");

      /* Check if port access methods are working properly */
      CPPUNIT_ASSERT_THROW (test_in = source1->input("invalid"),
                            eqnx::runtime_exception);
      CPPUNIT_ASSERT_THROW (test_in = sink->input("invalid"),
                            eqnx::runtime_exception);
      CPPUNIT_ASSERT_THROW (test_out = source1->output("invalid"),
                            eqnx::runtime_exception);
      CPPUNIT_ASSERT_THROW (test_out = sink->output("invalid"),
                            eqnx::runtime_exception);
      CPPUNIT_ASSERT_THROW ( (*sink)["invalid"],
                            eqnx::runtime_exception);
      CPPUNIT_ASSERT_THROW (sink->port("invalid"),
                            eqnx::runtime_exception);

      /* Manually create a connection, using the available methods */
      anch0 = (*sink)["in0"];
      anch1 = (*source0)["out"];

      /* These connections should fail */
      CPPUNIT_ASSERT_THROW (conn = connection::make_shared(anch0, anch0),
                            eqnx::runtime_exception);
      CPPUNIT_ASSERT_THROW (conn = connection::make_shared(anch1, anch1),
                            eqnx::runtime_exception);
      CPPUNIT_ASSERT_THROW (conn = connection::make_shared(anch0, anch1),
                            eqnx::runtime_exception);
      /* Check using the >> operator too */
      CPPUNIT_ASSERT_THROW (anch0 >> anch0, eqnx::runtime_exception);
      CPPUNIT_ASSERT_THROW (anch1 >> anch1, eqnx::runtime_exception);
      CPPUNIT_ASSERT_THROW (anch0 >>  anch1, eqnx::runtime_exception);

      /* Now these connections should be ok */
      conn = anch1 >> anch0;
      CPPUNIT_ASSERT(conn->src_kernel() == source0);
      CPPUNIT_ASSERT(conn->dst_kernel() == sink);
      CPPUNIT_ASSERT(conn->src_port() == source0->output("out"));
      CPPUNIT_ASSERT(conn->dst_port() == sink->input("in0"));

      (*d_outer_sched) += conn;

      /* Connection already exists and an exception should be raised */
      CPPUNIT_ASSERT_THROW ((*d_outer_sched) += anch1 >> anch0,
                            eqnx::runtime_exception);
    }

  }  // namespace core

}  // namespace eqnx
